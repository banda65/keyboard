//
//  ViewController.swift
//  keyboard
//
//  Created by David Banda on 13/01/17.
//  Copyright © 2017 David Banda. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var textField: UITextField!
    
    
    @IBAction func keyboard(_ sender: UITextField) {
        textField.resignFirstResponder()
    }
    

}

